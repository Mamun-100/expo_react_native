import React,{ useState } from 'react'
import { StyleSheet, Text, View, FlatList,TextInput,ScrollView } from 'react-native'
import DATASTORE from '../../constants/DATASTORE'

const AppFlatList = () => {
const [search, setSearch] = useState('')
let data = DATASTORE.data.filter((item)=>
    item.name.toLocaleLowerCase() == search.toLocaleLowerCase()
).map((i)=>{
    console.log(`i----------------`, i.fname)
    return (
       alert(i.fname +'\n' +i.mname)
    )
})
console.log(`data-----------==`, data)
    return (
        <View style={{ flex: 1, marginTop: '15%',borderRadius:15,marginBottom:'20%' }}>
            <View>
                <TextInput 
                    placeholder='Search'
                    style={{height:40,width:'90%',alignSelf:'center',paddingLeft:20,borderColor:'#333',borderRadius:20,borderWidth:1}}
                    value={search}
                    onChangeText={(value)=>setSearch(value)}
                />
            </View>
            <View>
                <FlatList
                    data={DATASTORE.data}
                    renderItem={({ item, index }) => {
                        console.log(`itemName===========`, item.name)
                        return (
                            <ScrollView style={{ backgroundColor: '#aaa', margin: 10, paddingVertical: '5%', paddingHorizontal: '5%' }}>
                                <Text>Name: {item.name}</Text>
                                <Text>FotherName: {item.fname}</Text>
                                <Text>MotherName: {item.mname}</Text>
                                <View style={{flexDirection:'row'}}>
                                    <View>
                                        <Text>Address:</Text>
                                    </View>
                                    <View>
                                        <Text>Villege:{item.address[0].v}</Text>
                                        <Text>Post: {item.address[0].p}</Text>
                                        <Text>District:{item.address[0].z}</Text>
                                    </View>
                                </View>
                            </ScrollView>
                        )
                    }}
                    keyExtractor={(i) => i.id}
                />
            </View>

        </View>
    )
}

export default AppFlatList

const styles = StyleSheet.create({})
