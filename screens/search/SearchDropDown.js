import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const SearchDropDown = (props) => {
    const {dataSource} = props
    return (
        <View style={{backgroundColor:'grey',margin:'5%',justifyContent:'center',alignItems:'center',width:'100%',paddingVertical:'5%'}}>
            <Text>find data</Text>
            {dataSource.map(item=>{
                return(
                    <Text style={{fontSize:25,color:'blue',paddingVertical:10}}>{item}</Text>
                )
            })}
        </View>
    )
}

export default SearchDropDown

const styles = StyleSheet.create({})
