import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, } from 'react-native'
import SearchDropDown from './SearchDropDown'
const data = ['mamun', 'salam', 'mijan', 'abir', 'kabir', 'zakir', 'jahangir']

const DropSearch = () => {
    const [dataSource] = useState(data);
    const [filtered, setFiltered] = useState(dataSource);
    const [searching, setSearching] = useState(false);

    const onSearch = (text) => {
        if (text) {
            setSearching(true);
            const temp = text.toLowerCase()
            const tempList = dataSource.filter(item => {
                if (item.match(temp))
                    return item
            })
            setFiltered(tempList)
        } else {
            setSearching(false)
            setFiltered(dataSource)
        }

    }
    return (
        <View style={styles.container}>
            <TextInput style={styles.textInput}
                placeholder='searching'
                placeholderTextColor="#fff"
                onChangeText={onSearch}

            />
            {/** dropdown search */}
            {
                searching &&
                <SearchDropDown dataSource={filtered} />
            }
            <View style={{ width: '100%', alignItems: 'center' }}>
                <Text style={{ fontSize: 20 }}>List of data</Text>
                {
                    dataSource.map(item => {
                        return (
                            <Text style={{ justifyContent: 'center', textAlign: 'center', padding: 5, borderRadius: 10, fontSize: 25, backgroundColor: '#aaa', marginVertical: 5, width: '100%' }}>
                                {item}
                            </Text>
                        )
                    })
                }
            </View>



        </View>
    )
}

export default DropSearch

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center', alignItems: 'center', marginTop: '10%'
    },
    textInput: { backgroundColor: '#000', width: '80%', height: 50, fontSize: 25, color: '#fff', borderRadius: 20, paddingLeft: 10 }
})
