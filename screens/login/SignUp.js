import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, StatusBar, TextInput } from 'react-native'
import Colors from '../../constants/Colors';
import { AntDesign } from '@expo/vector-icons';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const SignUp = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor={Colors.bodyPrimary} hidden={false} translucent={true} />
       
            <TouchableOpacity
                onPress={() => navigation.navigate('Login')}
                style={{ marginTop: '15%', paddingLeft: '5%' }}>
                <AntDesign name="arrowleft" size={30} color={Colors.whiteText} />
            </TouchableOpacity>
           
            <View style={styles.titleView}>
                <Text style={styles.titleName}>Welcome to tradly</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '10%', marginBottom: '6%' }}>
                <Text style={{ color: Colors.whiteText, fontSize: 18 }}>Signup to your account</Text>
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="First name"
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                />
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="last Name"
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                />
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="Email"
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                />
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="Phone No"
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                />
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="Password"
                    secureTextEntry={true}
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    maxLength={12}

                />
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    placeholder="Re-enter Password"
                    secureTextEntry={true}
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    maxLength={12}

                />
            </View>
            <TouchableOpacity style={styles.signBtn} onPress={() => navigation.navigate('SentOtp')}>
                <Text style={styles.btnText}>Create</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center',}}>
                <Text style={{ marginTop: '1.3%', color: Colors.whiteText, fontSize: 18 }}>Have an account? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={{ color: Colors.whiteText, fontWeight: '700', fontSize: 20, marginTop: '5%' }}>Sign in</Text></TouchableOpacity>
            </View>
        </View>
    )
}

export default SignUp

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.bodyPrimary,
        width: windowWidth,
        height: windowHeight,
    },
    titleView: {
        marginTop: '5%'
    },
    titleName: {
        color: Colors.whiteText,
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 25,
        fontWeight: '600'
    },
    inputView: {
        width: '84%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 30,
        paddingLeft: '5%',
        marginVertical: '2%'

    },
    inputText: {
        color: '#fff',
        fontSize: 18,
    },
    signBtn: {
        width: '84%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.boxWhite,
        borderRadius: 30,
        marginTop: '8%',
        marginBottom: '5%'

    },
    btnText: {
        color: Colors.lGreen,
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '500',
    },
})
