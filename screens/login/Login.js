import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, StatusBar, TextInput, TouchableOpacity, } from 'react-native'
import Colors from '../../constants/Colors';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Login = () => {
    const navigation = useNavigation();
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [password, setPassword] = useState('');
    const [passError, setPassError] = useState('');
    const [message, setMessage] = useState('');

    const signin = async () => {
        if (email!="" && password!="") {
            // alert('Thanks your for sign in.')
            await fetch('https://reqres.in/api/login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-type': 'application/json'
                },
                body:JSON.stringify({
                    email: email,
                    password: password
                })

            }).then((res) => res.json())
                .then((resData) => {
                    setMessage(resData.message)
                })
        }
        if (email != "") {
            // alert(email);
            setEmailError('');
        } else {
            setEmailError('Hey email should not be empty')
        }

        if (password != "") {
            alert(password);
            setPassError('');
        } else {
            setPassError('password should not be empty')
        }
    }
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor={Colors.bodyPrimary} hidden={false} translucent={true} />
            <View style={styles.titleView}>
                <Text style={styles.titleName}>Welcome to tradly</Text>
            </View>
            <View style={styles.title}>
                <Text style={styles.titleText}>Login to your account</Text>
            </View>
            <View style={[styles.inputView]}>
                <TextInput
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                    placeholder="Email"
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                />
            </View>
            <Text style={{ fontSize: 10, fontWeight: '700', color: '#f00', paddingLeft: '12%' }}>{emailError}</Text>
            <View style={[styles.inputView]}>
                <TextInput
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                    placeholder="Password"
                    secureTextEntry={true}
                    style={styles.inputText}
                    placeholderTextColor='#fff'
                    maxLength={12}
                />
            </View>
            <Text style={{ fontSize: 10, fontWeight: '700', color: '#f00', paddingLeft: '12%' }}>{passError}</Text>
            <TouchableOpacity style={styles.loginBtn}
                onPress={() => signin()}>
                <Text style={styles.btnText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.fpBtn}
                onPress={() => navigation.navigate('ForgetPassword')} >
                <Text style={styles.text}>Forgot your password</Text>
            </TouchableOpacity>
            <Text>{message}</Text>
            <View style={styles.acc}>
                <Text style={styles.accText}>Don't have an account? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                    <Text style={styles.signText}>Sign up</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.bodyPrimary,
        width: windowWidth,
        height: windowHeight,
    },
    titleView: {
        marginTop: '20%'
    },
    titleName: {
        color: Colors.whiteText,
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 25,
        fontWeight: '600'
    },
    inputView: {
        width: '84%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 30,
        paddingLeft: '5%',
        marginVertical: '3%'

    },
    inputText: {
        color: '#fff',
        fontSize: 18,
    },
    loginBtn: {
        width: '84%',
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.boxWhite,
        borderRadius: 30,
        marginTop: '10%',
        marginBottom: '10%'

    },
    btnText: {
        color: Colors.lGreen,
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '500',
    },
    fpBtn: {
        justifyContent: 'center',
        textAlign: 'center',
        marginBottom: '3%'
    },
    text: {
        color: Colors.whiteText,
        fontSize: 20,
        textAlign: 'center'
    },
    title: { justifyContent: 'center', alignItems: 'center', marginTop: '15%', marginBottom: '12%' },
    titleText: { color: Colors.whiteText, fontSize: 18 },
    acc: { flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', marginTop: '5%' },
    accText: { marginTop: '1.3%', color: Colors.whiteText, fontSize: 18 },
    signText: { color: Colors.whiteText, fontWeight: '700', fontSize: 20, marginTop: '5%' },


})