import React, { useState } from 'react'
import { StyleSheet, Text, View, StatusBar, Dimensions, TouchableOpacity, TextInput } from 'react-native'
import Colors from '../../constants/Colors';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Feather } from '@expo/vector-icons';


const ForgetPassword = () => {
    const navigation = useNavigation();
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [oldpassError, setOldpassError] = useState('');
    const [newPassError, setNewPassError] = useState('');
    const [hidePass, setHidePass] = useState('');
    const [hidePass1, setHidePass1] = useState('');

    const forgetPassword = () => {

    }
    const formHandle = () => {
        const validEmail = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
        if (email.length == 0) {
            setEmailError('Email is required');
        }
        else if (!validEmail.test(email)) {
            setEmailError('Email is invalid');
        }
        else if (email.indexOf(' ') >= 0) {
            setEmailError('Email cannot contain spaces');
        } else {
            setEmailError('');
        }

        if(oldPassword.length==0){
            setOldpassError('Old Password is require');
        }else{
            setOldpassError('')
        }

        if (newPassword.length == 0) {
            setNewPassError('New Password is required');
        } else if (newPassword.length < 8) {
            setNewPassError('New Passwod should be minimum 8 character');
        } else if (newPassword.indexOf(' ') >= 0) {
            setNewPassError('New Password cannot contain spaces');
        } else {
            setNewPassError('')
        }

    }


    return (
        <View style={s.container}>
            <StatusBar barStyle="dark-content" backgroundColor={Colors.bodyPrimary} hidden={false} translucent={true} />
            <TouchableOpacity
                onPress={() => navigation.navigate('Login')}
                style={{ marginTop: '15%', paddingLeft: '5%' }}>
                <AntDesign name="arrowleft" size={30} color={Colors.whiteText} />
            </TouchableOpacity>
            <View style={s.titleText}>
                <Text style={{ color: Colors.whiteText, fontSize: 18, fontFamily: 'mr' }}>Forget Passwod</Text>
            </View>
            <View style={[s.inputView, { borderWidth: 1, borderRadius: 20, borderColor: emailError.length > 0 ? '#f00' : '#fff' }]}>
                <TextInput
                    placeholder="Email Address"
                    style={s.inputText}
                    placeholderTextColor='#fff'
                    selectTextOnFocus={true}
                    returnKeyType='next'
                    keyboradType='email-address'
                    value={email}
                    onChangeText={(value) => setFname(value)}

                />
            </View>
            <Text style={s.tw}>{emailError}</Text>
            <View style={[s.inputView2, { borderWidth: 1, borderColor:oldpassError.length > 0 ? '#f00' : '#fff'}]}>
                <TextInput
                    placeholder="Old Password"
                    secureTextEntry={hidePass ? true : false}
                    style={s.inputText2}
                    placeholderTextColor='#fff'
                    maxLength={12}
                    returnKeyType='next'
                    value={oldPassword}
                    onChangeText={(value) => setOldPassword(value)}

                />
                <Feather name={hidePass ? 'eye-off' : 'eye'}
                    size={24} color="#fff"
                    style={s.eye}
                    onPress={() => setHidePass(!hidePass)}
                />
            </View>
            <Text style={s.tw}>{oldpassError}</Text>
            <View style={[s.inputView2, { borderWidth: 1, borderColor: newPassError.length > 0 ? '#f00' : '#fff' }]}>
                <TextInput
                    placeholder="New Password"
                    secureTextEntry={hidePass1 ? true : false}
                    style={s.inputText2}
                    placeholderTextColor='#fff'
                    maxLength={12}
                    returnKeyType='done'
                    value={newPassword}
                    onChangeText={(value) => setNewPassword(value)}

                />
                <Feather name={hidePass1 ? 'eye-off' : 'eye'}
                    size={24} color="#fff"
                    style={s.eye}
                    onPress={() => setHidePass1(!hidePass1)}
                />
            </View>
            <Text style={s.tw}>{newPassError}</Text>

            <TouchableOpacity style={s.btn}
            onPress={()=>formHandle()}
            >
                <Text style={s.btnText}>Confirm</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ForgetPassword

const s = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.bodyPrimary,
        width: windowWidth,
        height: windowHeight,
    },

    titleText: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '15%',
        marginBottom: '16%'
    },
    inputView: {
        width: '84%',
        height: 45,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 30,
        paddingLeft: '5%',
        marginTop: '8%'
    },
    inputView2: {
        width: '84%',
        height: 45,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
        borderRadius: 30,
        paddingLeft: '5%',
        marginTop: '3%'
    },

    eye: {
        alignSelf: 'center', marginRight: '1.8%',
    },

    inputText2: {
        color: '#fff',
        fontSize: 18,
        width: '90%',
        fontFamily: 'mr'
    },
    inputText: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'mr'
    },
    btn: {
        width: '84%',
        height: 45,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.boxWhite,
        borderRadius: 30,
        marginTop: '8%',
        marginBottom: '5%'
    },
    btnText: {
        color: Colors.lGreen,
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'mr'
    },
    tw:{
       
        paddingHorizontal:'10%',
        color:'red',
        fontWeight:'500'
        
    }
})
