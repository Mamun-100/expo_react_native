import React, { Component } from 'react'
import { Text, StyleSheet, View,Image, TouchableHighlight } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider';

const slider = [
    {
        key: 1,
        title: 'first',
        text: 'this is first text',
        image: require('../../assets/images/slider/a1.jpg')
    },
    {
        key: 2,
        title: 'second',
        text: 'this is second text',
        image: require('../../assets/images/slider/a2.jpg')
    },
    {
        key: 3,
        title: 'third',
        text: 'this is third text',
        image: require('../../assets/images/slider/a3.jpg')
    },
]

export default class SliderApp extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showRealApp: false
        }
    }

    _renderItem = ({ item }) => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>{item.title}</Text>
                <Image 
                source={item.image}
                />
                <Text>{item.text}</Text>
            </View>
        )
    }
    _renderNextButton = () => {
      return (
        <View style={styles.btn}>
          <Text style={styles.txt}>next</Text>
        </View>
      );
    };
  _renderNextButton = () => {
      return (
        <View style={styles.btn}>
          <Text>next</Text>
        </View>
      );
    };
    _renderDoneButton = () => {
      return (
        <View style={styles.btn}>
          <Text>finish</Text>
        </View>
      );
    };
    render() {
        if(this.state.showRealApp){
            return <SliderApp />
        }else
        return (
         
                <AppIntroSlider
                data={slider}
                renderItem={this._renderItem}
                renderDoneButton={this._renderDoneButton}
                renderNextButton={this._renderNextButton}
                activeDotStyle={{
                    backgroundColor:'#00ffff',
                    width:20,height:20
                }}
                bottomButton={true}
            />
         
        
        )
    }
   

   
}

const styles = StyleSheet.create({
btn:{justifyContent:'center',alignItems:'center',width:'90%',height:50,backgroundColor:"#ffff00"},
txt:{textAlign:'center'}
})
