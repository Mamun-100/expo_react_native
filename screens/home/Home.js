import { useNavigation } from '@react-navigation/native'
import React, { useState } from 'react'
import { Appearance, StyleSheet, Text, TouchableHighlight, View, ScrollView } from 'react-native'
import darkMode from '../../styles/darkMode'

const Home = () => {
    const navigation = useNavigation();
    const [theme, setTheme] = useState(Appearance.getColorScheme());

    Appearance.addChangeListener((scheme) => {
        setTheme(scheme.colorScheme)
    });
    
    return (
        <ScrollView style={theme == 'light' ? styles.container : darkMode.container}>
            <Text style={styles.titleNameStyle}>Login</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('Login')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>Login</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('SignUp')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>SignUp</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() =>navigation.navigate('DataFetch')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>DataFetch</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('UseEffectsExample')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>useEffects</Text>
                </TouchableHighlight>
            </View>
            <Text style={styles.titleNameStyle}>Slider</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('SliderApp')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>Slider</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() =>navigation.navigate('AppFlatList')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>AppFlatList</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ccc')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>cccc</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ddd')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>dddd</Text>
                </TouchableHighlight>
            </View>
            <Text style={styles.titleNameStyle}>Filter</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('AppFilter')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>AppFilter</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('SearchBar')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>SearchBar</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('DropSearch')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>DropSearch</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ddd')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>dddd</Text>
                </TouchableHighlight>
            </View>
            <Text style={styles.titleNameStyle}>Cart</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('CartApp')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>Cart</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('bbb')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>bbb</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ccc')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>cccc</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ddd')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>dddd</Text>
                </TouchableHighlight>
            </View>
            <Text style={styles.titleNameStyle}>Location</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => navigation.navigate('LocationApp')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>LocationSet</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('bbb')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>bbb</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ccc')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>cccc</Text>
                </TouchableHighlight>
                <TouchableHighlight
                    activeOpacity={.8}
                    underlayColor="#aaa"
                    onPress={() => alert('ddd')}
                    style={styles.columnStyle}
                >
                    <Text style={styles.textStyle}>dddd</Text>
                </TouchableHighlight>
            </View>
          
        


        </ScrollView>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        marginTop:'17%',
        marginBottom: '2%',
        backgroundColor: '#aaa'
    },
    columnStyle: {
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 3,
        borderColor: '#f4511e',
        borderRadius: 5,
        backgroundColor: '#f4511e',
        height: 40,
    },
    titleNameStyle: {
        textAlign: 'center', fontSize: 20, fontWeight: '700'
    },
    textStyle: {
        fontWeight: '700',
        fontSize: 15,
        color: '#fff'
    }
})
