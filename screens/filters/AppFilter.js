import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, SafeAreaView, FlatList } from 'react-native'
import DATASTORE from '../../constants/DATASTORE';

const listTab = [
    {
        status: 'All',
    },
    {
        status: 'Purple',
    },
    {
        status: 'Green',
    }
]
const data = [
    { name: 'Mamun', status: 'Green' },
    { name: 'Salam', status: 'Purple' },
    { name: 'Abir', status: 'Green' },
    { name: 'Kalam', status: 'Purple' },
    { name: 'Enamul', status: 'Green' },
]
const AppFilter = () => {  //100% work done
    const [status, setStatus] = useState('All');
    const [datalist, setDatalist] = useState(data)

    const setStatusFilter = status => {
        if (status !=='All') {
            setDatalist([...data.filter(e => e.status === status)])
        } else {
            setDatalist(data)
        }
        setStatus(status)
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.main}>
                {
                    listTab.map((e) => {
                        return (
                            <TouchableOpacity style={[styles.btn, status === e.status && styles.btnActive]}
                                onPress={() => setStatusFilter(e.status)}
                            >
                                <Text style={[styles.text, status === e.status && styles.textActive]}>
                                    {e.status}
                                </Text>
                            </TouchableOpacity>
                        )

                    })
                }
            </View>
            <FlatList
                data={datalist}
                keyExtractor={(e, i) => i.toString()}
                // ItemSeparatorComponent={separator}
                renderItem={({ item, index }) => {
                    return (
                        <View key={index} style={styles.itemContainer}>
                            <View style={styles.imgView}>
                                <Image
                                    source={require('../../assets/images/slider/a1.jpg')}
                                    style={{ width: 30, height: 30 }} />
                            </View>
                            <View style={styles.textmain}>
                                <Text>{item.name}</Text>
                            </View>
                            <View style={[styles.itemStatus, { backgroundColor: item.status === 'Purple' ? '#e58' : '#69c080' }]}>
                                <Text>{item.status}</Text>
                            </View>
                        </View>
                    )

                }}
            />
        </SafeAreaView>
    )
}

export default AppFilter

const styles = StyleSheet.create({
    container: {
        marginTop: '15%',
        paddingHorizontal: 10,
        justifyContent: 'space-around'
    },
    main: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#fff',
        paddingVertical: '2%'
    },
    btn: {
        backgroundColor: '#aaffaa'
    },
    btnActive: {
        backgroundColor: 'green'
    },
    text: {
        fontSize: 45, color: '#000'
    },
    textActive: {
        color: 'red'
    },
    itemContainer: {
        flexDirection: 'row',
        paddingVertical: 15,
        justifyContent: 'space-around'

    }, imgView: {

    },
    textmain: {

    },
    itemStatus: {
        backgroundColor: 'green'
    }
})
