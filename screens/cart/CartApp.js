import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Text, View } from 'react-native'
import ShoppingCart from './ShoppingCart'

export class CartApp extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ShoppingCart />
            </View>
        )
    }
}

export default CartApp
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})

