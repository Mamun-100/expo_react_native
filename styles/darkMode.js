import React from "react";
import { StyleSheet } from "react-native";
const darkMode = StyleSheet.create({
    container:{
        marginTop:'17%',
        marginBottom:'2%',backgroundColor:'#333'
        
    },
    columnStyle: {
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 3,
        borderColor: '#ddd',
        borderRadius: 5,
        backgroundColor: '#fff',
        height: 40,
    },
    titleNameStyle: {
        textAlign: 'center', fontSize: 20, fontWeight: '700'
    },
    textStyle: {
        fontWeight: '700',
        fontSize: 15,
        color: '#ff0'
    }
});
export default darkMode