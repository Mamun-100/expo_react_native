import { NavigationContainer, } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import React from 'react'
import { StyleSheet, Text, View, } from 'react-native'
import Home from '../screens/home/Home';
import Login from '../screens/login/Login';
import SignUp from '../screens/login/SignUp';
import SliderApp from '../screens/slider/SliderApp';
import SearchBar from '../screens/search/SearchBar'
import DropSearch from '../screens/search/DropSearch'
import CartApp from '../screens/cart/CartApp';
import LocationApp from '../screens/location/LocationApp';
import UseEffectsExample from '../hooks/UseEffectsExample';
import DataFetch from '../hooks/DataFetch';
import ForgetPassword from '../screens/login/ForgetPassword';
import AppFlatList from '../screens/flatList/AppFlatList';
import AppFilter from '../screens/filters/AppFilter';

const Stack = createNativeStackNavigator();

const Navigation = () => {


    return (

        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home"
                screenOptions={{
                    headerShown: false
                }} >
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Login" component={Login}
                // options={{
                //     title: 'Reverse It Solutions',
                //     headerStyle: { backgroundColor: '#f4511e' },
                //     headerTintColor: '#fff',
                //     headerTitleStyle: { fontWeight: 'bold', },
                //     headerTitleAlign: 'center',
                // }} 
                />
                <Stack.Screen name="SignUp" component={SignUp} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="ForgetPassword" component={ForgetPassword} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="SliderApp" component={SliderApp} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="SearchBar" component={SearchBar} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="DropSearch" component={DropSearch} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="CartApp" component={CartApp} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="LocationApp" component={LocationApp} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="UseEffectsExample" component={UseEffectsExample} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="DataFetch" component={DataFetch} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="AppFlatList" component={AppFlatList} options={{ headerTitleAlign: 'center' }} />
                <Stack.Screen name="AppFilter" component={AppFilter} options={{ headerTitleAlign: 'center' }} />

            </Stack.Navigator>
        </NavigationContainer>

    )
}

export default Navigation

const styles = StyleSheet.create({})
