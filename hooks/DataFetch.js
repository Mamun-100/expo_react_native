import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text,ActivityIndicator, FlatList } from 'react-native'

const DataFetch = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
   // console.log('-----------data', data);
    // const [todos, setTodos] = useState(null);
    // const fetchData = async () => {
    //     try {
    //         let response = await fetch('https://jsonplaceholder.typicode.com/todos');
    //         let res = await response.json();
    //         console.log(`res-------------`, res)
    //         return res.title;

    //     } catch (error) {
    //         console.error('error-----------',error)
    //     }

    // }

    // useEffect(() => {
    //     fetch("https://jsonplaceholder.typicode.com/todos")
    //         .then((res) => {
    //             return res.json();
    //         })
    //         .then((data) => {
    //             setTodos(data);
    //             console.log('todos-------', todos);
    //         })
    //     fetchData();
    // }, [])

    useEffect(() => {
        fetch('https://raw.githubusercontent.com/adhithiravi/React-Hooks-Examples/master/testAPI.json')
            .then((response) => response.json())
            .then((res) => setData(res))
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
    }, [])
    return (
        <View style={s.main}>
            <Text style={{fontWeight:'700',fontSize:20,color:'#f00'}}>DataFetch</Text>
            {/* {
                todos && todos.map((item) => {
                    return <View>
                        <Text key={item.id}>{item.title}</Text>
                    </View>
                })
            } */}
            <View>
                {isLoading ? <ActivityIndicator /> : (
                    <FlatList
                        data={data}
                        keyExtractor={({ id }, index) => id}
                        renderItem={({ item }) =>{
                            return (
                                <Text>{item.title}</Text>
                            )
                        } }
                    />
                )}
            </View>
        </View>
    )
}

export default DataFetch
const s = StyleSheet.create({
    main: {
      marginTop:'18%', justifyContent: 'center', alignItems: 'center'
    }
})
