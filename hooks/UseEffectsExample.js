import React,{ useState,useEffect } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

const UseEffectsExample = () => {
    const [count, setCount] = useState(0);
    
    // const handleonPress = () =>{
    //     setCount(count + 2);
    // }
    useEffect(()=>{
        console.log('useEffect');
    },[])

    return (
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            {console.log('render')}
            <Text>UseEffectsExample</Text>
            <Text>{count}</Text>
            <Button onPress={()=> setCount(count + 2)} title='+'/>
        
        </View>
    )
}

export default UseEffectsExample

const styles = StyleSheet.create({})
