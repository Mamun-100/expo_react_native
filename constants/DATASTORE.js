export const DATASTORE = {
    data :[
        {
            id: 1,
            name:'Mamunur Rashid',
            fname:'Sukur Ali',
            mname:'Maleka Begum',
            address:[
                {
                    v:'Zigabari',
                    p:'Sundargong',
                    z:'Gaibantha'
                }
            ],
        },
        {
            id: 2,
            name:'Harunur Rashid',
            fname:'Harun Ali',
            mname:'Halima Begum',
            address:[
                {
                    v:'Sipaibag',
                    p:'Khilgaon',
                    z:'Dhaka-1219'
                }
            ],
        },
        {
            id: 3,
            name:'Hasan',
            fname:'Habibur rahman',
            mname:'Habiba Begum',
            address:[
                {
                    v:'bamondanga',
                    p:'Sundargonj',
                    z:'Gaibantha'
                }
            ],
        },
        {
            id: 4,
            name:'Sopon Mia',
            fname:'Malek Mia',
            mname:'Monira',
            address:[
                {
                    v:'Dhonia',
                    p:'Choddgrame',
                    z:'Cumilla',
                }
            ],
        },
    ]
}
export default DATASTORE;