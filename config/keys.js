// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBsuGbFS7QnsfTYtPnXSTDwkCEOKGNr9a4",
  authDomain: "expo-project-1d110.firebaseapp.com",
  projectId: "expo-project-1d110",
  storageBucket: "expo-project-1d110.appspot.com",
  messagingSenderId: "36623560388",
  appId: "1:36623560388:web:0f6321b30fbf061b4f2391"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);