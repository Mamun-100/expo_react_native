import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Colors from '../constants/Colors'

const CustomButtom = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                height: 40,
                justifyContent: 'center',
                alignItems: 'center', backgroundColor: Colors.bodyPrimary,
                borderRadius: 20
            }}>
            <Text style={{ color: '#fff', fontSize: 20,fontFamily:'mr' }}>{props.btnName}</Text>
        </TouchableOpacity>
    )
}

export default CustomButtom

const styles = StyleSheet.create({})