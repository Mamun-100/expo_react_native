import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Header = () => {
    return (
        <View>
            <Text>Reverse IT Solutions</Text>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({})
