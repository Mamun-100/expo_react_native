import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const AppName = () => {
    return (
        <View>
            <Text style={{fontSize: 25, justifyContent:'center',textAlign:'center',fontWeight:'700'}}>Reverse It Solutions</Text>
        </View>
    )
}

export default AppName

const styles = StyleSheet.create({})
